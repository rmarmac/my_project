#pragma once
#ifdef UTILS_EXPORTS
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif


void DLL SwapINT(int*, int, int);
void DLL SwapFLOAT(float*, int, int);
void DLL QuickSortINT(int*, int);
void DLL QuickSortFLOAT(float*, int);
