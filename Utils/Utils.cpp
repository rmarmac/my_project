#include "Utils.h"

void DLL SwapINT(int* array, int index1, int index2) {
	array[index1] += array[index2];
	array[index2] = array[index1] - array[index2];
	array[index1] = array[index1] - array[index2];
}
void DLL SwapFLOAT(float* array, int index1, int index2) {
	array[index1] += array[index2];
	array[index2] = array[index1] - array[index2];
	array[index1] = array[index1] - array[index2];
}
void DLL QuickSortINT(int* array, int tamanho) {
	if (tamanho < 2)
		return;
	int pivot = array[0];
	int posicao_pivot = 0;
	for (int i = 1; i < tamanho; i++) {
		if (array[i] < array[posicao_pivot]) {
			SwapINT(array, posicao_pivot, posicao_pivot + 1);
			posicao_pivot++;
			if (posicao_pivot != i)
				SwapINT(array, posicao_pivot - 1, i);
		}
	}
	QuickSortINT(array, posicao_pivot);
	QuickSortINT(array + posicao_pivot + 1, tamanho - posicao_pivot - 1);
}
void DLL QuickSortFLOAT(float* array, int tamanho) {
	if (tamanho < 2)
		return;
	float pivot = array[0];
	int posicao_pivot = 0;
	for (int i = 1; i < tamanho; i++) {
		if (array[i] < array[posicao_pivot]) {
			SwapFLOAT(array, posicao_pivot, posicao_pivot + 1);
			posicao_pivot++;
			if (posicao_pivot != i)
				SwapFLOAT(array, posicao_pivot - 1, i);
		}
	}
	QuickSortFLOAT(array, posicao_pivot);
	QuickSortFLOAT(array + posicao_pivot + 1, tamanho - posicao_pivot - 1);
}